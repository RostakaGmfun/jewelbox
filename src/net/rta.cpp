#include <jewelbox/net/rta.hpp>
#include <linux/netlink.h>
#include <asm/types.h>

#include <numeric>
#include <iostream>

namespace jewelbox
{

namespace net
{

rta::rta(const rtattr *start, std::size_t length): dirty_flag_(false)
{
    parse_rtattr(start, length);
}

void rta::reset(const rtattr *start, std::size_t length) noexcept
{
    dirty_flag_ = false;
    attribute_array_cache_.clear();
    attributes_.clear();
    parse_rtattr(start, length);
}

void rta::parse_rtattr(const rtattr *start, std::size_t length) noexcept
{
    if (start != nullptr) {
        while (RTA_OK(start, length)) {
            const uint8_t *data_start = static_cast<std::uint8_t*>(RTA_DATA(start));
            attributes_.emplace(start->rta_type, std::vector<std::uint8_t>(data_start,
                        data_start + RTA_PAYLOAD(start)));
            start = RTA_NEXT(start, length);
        }
        dirty_flag_ = true;
        update_attribute_array_cache();
    }
}

void rta::update_attribute_array_cache() const noexcept
{
    if (!dirty_flag_) {
        return;
    }

    attribute_array_cache_.clear();
    std::size_t total_size = std::accumulate(attributes_.begin(), attributes_.end(),
            0, [] (const std::size_t &a, const decltype(attributes_)::value_type &b) {
                return a + RTA_LENGTH(b.second.size());
            });
    attribute_array_cache_.reserve(total_size);
    rtattr attr_header;
    for (const auto &attr : attributes_) {
        attr_header.rta_type = attr.first;
        attr_header.rta_len = attr.second.size();
        const uint8_t *header_start = reinterpret_cast<const uint8_t*>(&attr_header);
        std::copy(header_start, header_start + sizeof(attr_header),
                std::back_inserter(attribute_array_cache_));
        std::copy(attr.second.begin(), attr.second.end(),
                std::back_inserter(attribute_array_cache_));
    }
    dirty_flag_ = false;
}

std::tuple<std::size_t, const void *> rta::get(rta_type_t type) const noexcept
{
    auto attr = attributes_.find(type);
    if (attr == attributes_.end()) {
        return { 0, nullptr };
    }

    return std::make_tuple(attr->second.size(), attr->second.data());
}

const rtattr *rta::data() const noexcept
{
    update_attribute_array_cache();
    return reinterpret_cast<const rtattr *>(attribute_array_cache_.data());
}

std::size_t rta::size() const noexcept
{
    update_attribute_array_cache();
    return attribute_array_cache_.size();
}

void rta::add(rta_type_t type, const void *attr, std::size_t size) noexcept
{
    attributes_[type] = std::vector<std::uint8_t>(reinterpret_cast<const std::uint8_t*>(attr),
            reinterpret_cast<const std::uint8_t*>(attr) + size);
    dirty_flag_ = true;
}

} // namespace net

} // namespace jewelbox
