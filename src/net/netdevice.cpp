#include <jewelbox/net/netdevice.hpp>
#include <jewelbox/net/netlink_client.hpp>

#include <sys/socket.h>
#include <linux/rtnetlink.h>
#include <linux/if.h>

#include <cstring>
#include <iomanip>
#include <iostream>

std::ostream &operator<<(std::ostream &os, const jewelbox::net::hw_addr_t &hwaddr)
{
    std::ios::fmtflags f( os.flags() );
    for (std::size_t i = 0; i < hwaddr.size()-1; i++) {
        os << std::setfill('0') << std::setw(2) << std::hex << (int)hwaddr[i] << ':';
    }
    os << std::setfill('0') << std::setw(2) << std::hex << (int)hwaddr[hwaddr.size()];
    os.flags(f);
    return os;
}


namespace jewelbox
{

namespace net
{

netdevice::netdevice(int index, std::shared_ptr<netlink_channel> nl_chan):
    index_(index), nl_channel_(nl_chan), nl_client_(new netlink_client), if_config_(new ifinfomsg)
{
    fetch_data();
}

netdevice::netdevice(netdevice &&other)
{
    *this = std::move(other);
}

netdevice &netdevice::operator=(netdevice &&other)
{
    index_ = other.index_;
    other.index_ = -1;
    nl_channel_ = other.nl_channel_;
    other.nl_channel_.reset();
    nl_client_ = other.nl_client_;
    other.nl_client_ = nullptr;
    if_config_ = other.if_config_;
    other.if_config_ = nullptr;
    if_attributes_ = other.if_attributes_;
    other.if_attributes_.reset(nullptr, 0);
    return *this;
}

netdevice::~netdevice()
{
    if (nl_client_ != nullptr) {
        delete nl_client_;
    }

    if (if_config_ != nullptr) {
        delete if_config_;
    }
}

bool netdevice::fetch_data() noexcept
{
    ifinfomsg request;
    request.ifi_family = AF_PACKET;
    request.ifi_index = index_;
    request.ifi_type = 1; // TODO: ARPHDR_ETHER definition is missing?
    request.ifi_change = 0xFFFFFFFF;
    if (!nl_client_->send_request(nl_channel_, 0, RTM_GETLINK, &request)) {
        return false;
    }
    auto responses = nl_client_->receive_response(nl_channel_, RTM_NEWLINK);
    if (responses.size() != 1) {
        return false;
    }
    const ifinfomsg *data = reinterpret_cast<decltype(data)>(NLMSG_DATA(responses[0]));
    std::copy(data, data + 1, if_config_);
    if_attributes_.reset(IFLA_RTA(data), responses[0]->nlmsg_len);
    return false;
}

optional_t<std::string> netdevice::get_name() const noexcept
{
    auto name = if_attributes_.get(IFLA_IFNAME);
    if (std::get<0>(name) == 0) {
        return {};
    }
    return std::string(reinterpret_cast<const char*>(std::get<1>(name)));
}

bool netdevice::is_up() const noexcept
{
    return if_config_->ifi_flags & IFF_UP;
}

optional_t<std::size_t> netdevice::get_mtu() const noexcept
{
    auto mtu = if_attributes_.get(IFLA_MTU);
    if (std::get<0>(mtu) == 0) {
        return {};
    }
    // TODO: deduce integral type from size of attribute data
    return *reinterpret_cast<const uint32_t*>(std::get<1>(mtu));
}

optional_t<hw_addr_t> netdevice::get_hw_addr() const noexcept
{
    auto addr = if_attributes_.get(IFLA_ADDRESS);
    if (std::get<0>(addr) == 0) {
        return {};
    }
    hw_addr_t ret;
    const std::uint8_t *start = reinterpret_cast<decltype(start)>(std::get<1>(addr));
    std::copy(start, start + std::get<0>(addr), ret.begin());
    return ret;
}

netdevice_type netdevice::get_type() const noexcept
{
    // TODO: Extend this
    auto flags = if_config_->ifi_flags;
    if (flags & IFF_LOOPBACK) {
        return netdevice_type::netdevice_lo;
    } else {
        return netdevice_type::netdevice_eth;
    }
}

} // namespace net

} // namespace jewelbox
