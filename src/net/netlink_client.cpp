#include <jewelbox/net/netlink_client.hpp>

#include <stdexcept>
#include <iostream>
#include <cstring>

#include <sys/socket.h>
#include <unistd.h>
#include <linux/rtnetlink.h>

namespace jewelbox
{

namespace net
{

synchronous_netlink_channel::synchronous_netlink_channel(pid_t pid, int protocol_family):
    netlink_channel(pid, protocol_family)
{
    sockfd_ = ::socket(AF_NETLINK, SOCK_RAW, get_protocol_family());
    if (sockfd_ < 0) {
        throw std::runtime_error("Failed to open NETLINK_ROUTE socket");
    }
    sockaddr_nl netlink_address{};
    netlink_address.nl_family = AF_NETLINK;
    netlink_address.nl_pid = get_pid();

    if (::bind(sockfd_, reinterpret_cast<sockaddr*>(&netlink_address),
                sizeof(sockaddr_nl)) < 0) {
        ::close(sockfd_);
        throw std::runtime_error("Failed to bind to pid " + std::to_string(get_pid()));
    }
}

synchronous_netlink_channel::~synchronous_netlink_channel()
{
    if (sockfd_ > 0) {
        ::close(sockfd_);
    }
}


optional_t<std::size_t> synchronous_netlink_channel::write(iovec *iov,
        std::size_t iov_length) noexcept
{
    if (iov== nullptr || iov_length == 0) {
        return {};
    }

    sockaddr_nl kernel_addr{};
    kernel_addr.nl_family = AF_NETLINK;

    msghdr message{};
    message.msg_iov = iov;
    message.msg_iovlen = iov_length;
    message.msg_name = &kernel_addr;
    message.msg_namelen = sizeof(kernel_addr);

    int ret = ::sendmsg(sockfd_, &message, 0);
    if (ret < 0) {
        return {};
    }
    return static_cast<std::size_t>(ret);
}

optional_t<std::size_t> synchronous_netlink_channel::read(std::uint8_t *buffer,
        std::size_t buffer_size) noexcept
{
    if (buffer == nullptr || buffer_size == 0) {
        return {};
    }

    sockaddr_nl kernel_addr{};
    kernel_addr.nl_family = AF_NETLINK;
    auto kaddr_size = sizeof(kernel_addr);

    int ret = ::recvfrom(sockfd_, buffer, buffer_size, MSG_DONTWAIT,
            reinterpret_cast<sockaddr*>(&kernel_addr),
            reinterpret_cast<socklen_t *>(&kaddr_size));
    if (ret < 0) {
        return {};
    }
    return static_cast<std::size_t>(ret);
}

bool netlink_client::send_request(netlink_channel_ptr chan, int nlm_flags, int req_type,
        const std::uint8_t *data, std::size_t data_size) noexcept
{
    if (chan == nullptr || data == nullptr || data_size == 0) {
        return false;
    }

    static int request_seqnum = 1;

    nlmsghdr netlink_header{};
    netlink_header.nlmsg_len = sizeof(netlink_header) + data_size;
    netlink_header.nlmsg_type = req_type;
    netlink_header.nlmsg_seq = request_seqnum++;
    netlink_header.nlmsg_flags = NLM_F_REQUEST | nlm_flags;
    netlink_header.nlmsg_pid = chan->get_pid();

    iovec request_data[2] = {
        {&netlink_header, sizeof(nlmsghdr)},
        // Folks say that casting out consntess is OK here
        {reinterpret_cast<void*>(const_cast<std::uint8_t*>(data)), data_size}
    };

    auto result = chan->write(request_data, 2);
    return result && result.get() == sizeof(nlmsghdr) + data_size;
}

std::vector<const nlmsghdr *> netlink_client::receive_response(netlink_channel_ptr chan,
        int resp_type) noexcept
{
    std::vector<const nlmsghdr *> result;
    bool message_end = false;
    constexpr auto chunk_size = 2 << 12; // 8kb
    std::vector<std::uint8_t> reply_buffer;
    std::size_t reply_iter = 0;
    reply_buffer.resize(chunk_size);
    do {
        int additional_space = chunk_size - reply_buffer.size() + reply_iter;
        reply_buffer.resize(reply_buffer.size() + additional_space);
        auto ret = chan->read(&reply_buffer[reply_iter], chunk_size);
        if (!ret) {
            return result;
        }
        int buf_size = chunk_size;
        nlmsghdr *cur_msg = reinterpret_cast<decltype(cur_msg)>(&reply_buffer[reply_iter]);
        while (NLMSG_OK(cur_msg, buf_size)) {
            if (cur_msg->nlmsg_type == NLMSG_DONE) {
                message_end = true;
                break;
            } else if (cur_msg->nlmsg_type == NLMSG_ERROR) {
                message_end = true;
                break;
            } else if (cur_msg->nlmsg_type == resp_type) {
                // This type of message was requested by user, save it into
                // internal buffer and put pointer to the start
                // of the message into the result array.
                response_cache_.push_front({reinterpret_cast<const uint8_t *>(cur_msg),
                            reinterpret_cast<const uint8_t *>(cur_msg) + NLMSG_ALIGN(cur_msg->nlmsg_len)});
                result.emplace_back(reinterpret_cast<const nlmsghdr*>(response_cache_.front().data()));

                // NLMSG_DONE is sent only for multipart responses.
                if (!(cur_msg->nlmsg_flags & NLM_F_MULTI)) {
                    message_end = true;
                    break;
                }
                // All other types (incuding NLMSG_ERROR message) are currently
                // silently ignored.
                // TODO: implement sophisticated processing of Netlink messages including
                // robust error handling, ACK-from-kernel handling
                // and use of user-provided buffer for filtered messages.
            }
            // Advance the reply_buffer iterator to the next message
            reply_iter += NLMSG_ALIGN(cur_msg->nlmsg_len);
            cur_msg = NLMSG_NEXT(cur_msg, buf_size);
        }
    } while (!message_end);

    return result;
}

} // namespace net

} // namespace jewelbox
