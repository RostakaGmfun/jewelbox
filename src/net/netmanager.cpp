#include <jewelbox/net/netmanager.hpp>
#include <jewelbox/net/rta.hpp>

#include <unistd.h>
#include <linux/rtnetlink.h>
#include <sys/socket.h>
#include <fcntl.h>

namespace jewelbox
{

namespace net
{

netmanager::netmanager():
nl_channel_(std::make_shared<synchronous_netlink_channel>(::getpid(), NETLINK_ROUTE))

{ }

std::vector<std::string> netmanager::query_devices() noexcept
{
    std::vector<std::string> result;
    rtgenmsg request_data;
    request_data.rtgen_family = AF_PACKET;
    if (!rtnet_client_.send_request(nl_channel_, NLM_F_DUMP,
                RTM_GETLINK, &request_data)) {
        return result;
    }
    auto  responses = rtnet_client_.receive_response(nl_channel_, RTM_NEWLINK);
    if (responses.size() == 0) {
        return result;
    }
    for (const auto &resp : responses) {
        ifinfomsg *iface = reinterpret_cast<decltype(iface)>(NLMSG_DATA(resp));
        rta rta_response(IFLA_RTA(iface), resp->nlmsg_len);
        auto name_attr = rta_response.get(IFLA_IFNAME);
        if (std::get<0>(name_attr) == 0) {
            continue;
        }
        std::string name = reinterpret_cast<const char *>(std::get<1>(name_attr));
        result.emplace_back(name);
        device_cache_[name] = iface->ifi_index;
    }

    return result;
}

netdevice_ptr netmanager::get_device(const std::string &name) noexcept
{
    auto index = device_cache_.find(name);
    if (index == device_cache_.end()) {
        // TODO: update the cache and check again
        return {};
    }
    return std::make_shared<netdevice>(index->second, nl_channel_);
}

} // namespace net

} // namespace jewelbox
