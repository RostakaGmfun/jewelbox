#include <jewelbox/box.hpp>

#include <sched.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/mount.h>
#include <unistd.h>
#include <fcntl.h>

#include <iostream>
#include <string>
#include <cstring>

namespace jewelbox
{

box::box(const box_env &env): env_(env)
{ }

box::~box()
{
    destroy();
}

bool box::start()
{
    destroy();

    int map_prot = PROT_READ | PROT_WRITE;
    int map_flags = MAP_PRIVATE | MAP_ANONYMOUS;

    static const std::size_t stack_size = sysconf(_SC_PAGESIZE)*2;

    void *stack_ptr= ::mmap(nullptr, stack_size, map_prot, map_flags, 0, 0);

    if (stack_ptr == MAP_FAILED) {
        return false;
    }

    stack_ = std::make_tuple(stack_pointer_t(stack_ptr), stack_size);

    int flags = CLONE_NEWUSER | CLONE_NEWPID | CLONE_NEWNS | CLONE_NEWUTS | SIGCHLD;

    // TODO: Think of container logging mechanism
    // (it is not good for library to trash stdout/stderr).
    const auto box_launcher = [] (void *args) -> int {
        // Basic tasks of init process:
        // * Chroot into unique directory
        // * Setup signal handler
        // * Mount procfs
        // * Drop privileges and capabilities
        // * Watch orphaned processes

        auto env = reinterpret_cast<std::add_pointer<jewelbox::box_env>::type>(args);

        if (!env->deploy()) {
            ::exit(EXIT_FAILURE);
        }

        // Busy-looping is not cool at all :c
        // TODO: Sleep periodically instead of looping
        // and catch SIGCHLD and SIGTERM signals.
        while (box_env::sigterm_flag != SIGTERM) {
            int status;
            pid_t pid = ::waitpid(-1, &status, 0);
            if (pid > 0) {
                std::cout << "Child " << pid << " exited with " << WEXITSTATUS(status) << '\n';
            }
        }
        env->cleanup();
        ::exit(EXIT_SUCCESS);
    };

    int pid = ::clone(box_launcher, std::get<0>(stack_) + stack_size, flags, &env_);
    if (pid < 0) {
        return false;
    }

    init_process_ = process(pid);

    const std::string procpath = "/proc/" + std::to_string(pid) + "/ns/";
    for (const auto &ns : { "user", "pid", "mnt", "uts" }) {
        std::string nspath = procpath + ns;
        int fd = ::open(nspath.c_str(), O_RDONLY);
        if (fd < 0) {
            return false;
        }
        ns_fds_.push_back(fd);
    }
    return true;
}

int box::destroy() noexcept
{
    int ret = -1;

    if (init_process_) {
        init_process_.get().send_signal(signal::sigterm);
        ret = init_process_.get().wait();
        init_process_.reset();
    }

    if (std::get<0>(stack_) != nullptr) {
        ::munmap(std::get<0>(stack_), std::get<1>(stack_));
    }

    for (int fd : ns_fds_) {
        ::close(fd);
    }

    return ret;
}

} // namespace jewelbox
