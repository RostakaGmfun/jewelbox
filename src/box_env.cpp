#include <jewelbox/box_env.hpp>

#include <sys/mount.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <cstring>
#include <cstdlib>
#include <iostream>
#include <random>

namespace jewelbox
{

std::sig_atomic_t box_env::sigterm_flag;

box_env::box_env(const std::string &box_name, const std::string &hostname,
            const std::map<std::string, std::string> &mount_points): host_name_(hostname),
    mount_points_(mount_points)
{
    // This does not look as robust is it should be
    // The current directory may change over9000 times before
    // the environment will be deployed.
    // Furthermore, it'd be nice to deploy same environment
    // across different boxes and directories.
    root_path_ = get_current_dir_name() + std::string("/.");
    if (box_name.empty()) {
        std::random_device rd;
        std::mt19937 generator(rd());
        root_path_ += std::to_string(std::uniform_int_distribution<std::size_t>()(generator));
    } else {
        root_path_ += box_name;
    }
}

bool box_env::deploy()
{
    // TODO: configurable logging
    if (::mkdir(root_path_.c_str(), 0777) < 0 && errno != EEXIST) {
        std::cerr << ::strerror(errno) << '\n';
        return false;
    }

    if (::chdir(root_path_.c_str()) < 0) {
        std::cerr << ::strerror(errno) << '\n';
        return false;
    }

    if (::mkdir("./proc", 0555) < 0 && errno != EEXIST) {
        std::cerr << "Failed to create /proc: " << ::strerror(errno) << '\n';
        return false;
    }

    if (::mount("./proc", "/proc", "proc", 0, nullptr) < 0) {
        std::cerr << "Failed to mount procfs into /proc: " << ::strerror(errno) << '\n';
        return false;
    }

    // Setup uid-map
    int fd = ::open(std::string("/proc/" + std::to_string(::getpid()) + "/uid_map").c_str(), O_RDWR);
    if (fd < 0) {
        std::cerr << "Failed to update uid_map: " << ::strerror(errno) << '\n';
        return false;
    }

    const std::string uid_map = "0 1000 1\n";
    if (::write(fd, uid_map.c_str(), uid_map.length()) < 0) {
        std::cerr << "Failed to udpate uid_map: " << ::strerror(errno) << '\n';
        ::close(fd);
        return false;
    }

    ::close(fd);

    std::signal(SIGTERM, [] (int sig) { box_env::sigterm_flag = sig; });

    if (!host_name_.empty()) {
        if (::sethostname(host_name_.c_str(), host_name_.length()) < 0) {
            std::cerr << "Failed to change hostname: " << ::strerror(errno) << '\n';
            return false;
        }
    }

    if (!mount_filesystems()) {
        unmount_filesystems();
        return false;
    }

    if (::chroot(".") < 0) {
        std::cerr << ::strerror(errno) << '\n';
        return false;
    }

    return true;
}

bool box_env::enter()
{
    if (::chdir(root_path_.c_str()) < 0) {
        std::cerr << ::strerror(errno) << '\n';
        return false;
    }

    if (::chroot(".") < 0) {
        std::cerr << ::strerror(errno) << '\n';
        return false;
    }

    return true;
}

void box_env::cleanup()
{
    ::umount("/proc");
    ::rmdir("/proc");
    unmount_filesystems();
}

bool box_env::mount_filesystems() noexcept
{
    // TODO: check each mountpoint for existense
    const auto try_mount = [] (const std::string &from, const std::string &to) {
        int ret;
        for (const auto &fstype : { "ext3", "ext4" }) {
            ret = ::mount(from.c_str(), to.c_str(), fstype, MS_BIND, nullptr);
            if (ret == 0) {
                return ::mount(from.c_str(), to.c_str(), fstype,
                        MS_BIND | MS_REMOUNT | MS_RDONLY, nullptr);
            }
        }
        return ret;
    };
    for (const auto &mpt : mount_points_) {
        // TODO: use boost::filesystem for more robust filepath handling
        const std::string target_path = root_path_ + mpt.second;
        if (::mkdir(target_path.c_str(), 0777) < 0 && errno != EEXIST) {
            std::cerr << "Failed to make directory " << target_path << ": " <<
                ::strerror(errno) << '\n';
            return false;
        }
        if (try_mount(mpt.first, target_path) < 0) {
            std::cerr << "Failed to mount " << mpt.first << " into " << target_path  <<
                ": " << ::strerror(errno) << '\n';
            return false;
        }
    }
    return true;
}

void box_env::unmount_filesystems() noexcept
{
    for (const auto &mpt : mount_points_) {
        ::umount(mpt.second.c_str());
        ::rmdir(mpt.second.c_str());
    }
}

} // namespace jewelbox
