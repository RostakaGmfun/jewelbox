#include <jewelbox/process.hpp>

#include <sys/wait.h>

namespace jewelbox
{

process::process(pid_t pid): pid_(pid)
{}

int process::wait()
{
    int status;
    if (::waitpid(pid_, &status, 0) == -1) {
        return -1;
    }
    return WEXITSTATUS(status);
}

void process::send_signal(signal sig)
{
    ::kill(pid_, static_cast<int>(sig));
}

} // namespace jewelbox
