#include <iostream>
#include <cstdlib>

#include <jewelbox/box.hpp>
#include <jewelbox/net/netmanager.hpp>

static void print_dev_info(const jewelbox::net::netdevice_ptr dev)
{
    using jewelbox::net::netdevice_type;
    static const std::map<netdevice_type, std::string> devtype_names = {
        { netdevice_type::netdevice_lo, "Loopback" },
        { netdevice_type::netdevice_eth, "Ethernet" }
    };
    const auto devtype_to_string = [&] (netdevice_type t) {
        auto name = devtype_names.find(t);
        if (name == devtype_names.end()) {
            return "Unknown";
        }
        return name->second.c_str();
    };

    std::cout << dev->get_index() << ": " << dev->get_name().get() <<
        (dev->is_up() ? " UP" : " DOWN") << '\n';
    std::cout << '\t' << "Type: " << devtype_to_string(dev->get_type()) << '\n';
    std::cout << '\t' << "MTU: " << dev->get_mtu().get() << '\n';
    std::cout << '\t' << "hwaddr: " << dev->get_hw_addr().get() << '\n';
}

int main()
{
    jewelbox::box example(jewelbox::box_env("example-box", "jewelbox"));

    if (!example.start()) {
        std::cerr << "Failed to start container\n";
        return EXIT_FAILURE;
    }

    std::cout << example.get_init_process().get_pid() << '\n';
    std::cin.get();
    std::cout << "Child exited with: " << example.execute([] {
            jewelbox::net::netmanager net_manager;
            for (const auto &dev : net_manager.query_devices()) {
                print_dev_info(net_manager.get_device(dev));
            }
            char hostname[20];
            ::gethostname(hostname, sizeof(hostname)-1);
            std::cout << "Hello world from pid " << getpid() << "@" <<  hostname << '\n';
    }).get().wait() << '\n';

    std::cout << "Container exited with " << example.destroy() << '\n';

    return EXIT_SUCCESS;
}
