#ifndef JEWELBOX_PROCESS_HPP
#define JEWELBOX_PROCESS_HPP

#include <sys/types.h>
#include <signal.h>

namespace jewelbox
{

enum class signal
{
    sigint = SIGINT,
    sigkill = SIGKILL,
    sigterm = SIGTERM,
    sigstop = SIGSTOP,
    sigcont = SIGCONT
};

class process
{
public:
    process(pid_t pid);

    int wait();

    void send_signal(signal sig);

    pid_t get_pid() const noexcept { return pid_; }

private:
    pid_t pid_;
};

} // namspace jewelbox

#endif // JEWELBOX_PROCESS_HPP
