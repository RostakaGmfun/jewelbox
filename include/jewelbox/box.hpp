#ifndef JEWELBOX_BOX_HPP
#define JEWELBOX_BOX_HPP

#include <jewelbox/process.hpp>
#include <jewelbox/box_env.hpp>

#include <unistd.h>
#include <sys/wait.h>

#include <tuple>
#include <vector>
#include <type_traits>
#include <boost/optional.hpp>

namespace jewelbox
{

using stack_pointer_t = std::uint8_t*;
using stack_handle_t = std::tuple<stack_pointer_t, std::size_t>;

template <typename T>
using optional_t = boost::optional<T>;

/**
 * Represents a container -- a set of Linux namespaces.
 */
class box
{
public:

    box(const box_env &env = {});
    ~box();

    /**
     * Starts the container by creating configured set of namespaces
     * and launching the init process within them.
     *
     * @return Success of operation.
     */
    bool start();

    /**
     * Deinitializes the container by killing the init process.
     *
     * @return init process exit code.
     * @retval -1 Cotainer has not been started.
     */
    int destroy() noexcept;

    /**
     * Retrieve reference to the container's init process.
     *
     * @note If the container has not been started successfully, the exception is thrown.
     */
    process &get_init_process()
    {
        if (init_process_) {
            return init_process_.get();
        } else {
            throw std::runtime_error("Box has not been started yet");
        }
    }

    stack_handle_t get_stack_ptr() const noexcept { return stack_; }

    std::string get_root_path() const noexcept { return env_.get_root_path(); };

    /**
     * Launches new process within existing container.
     *
     * @tparam Func Type of functor.
     * @param  func Functor.
     *
     * @return Process or none
     */
    template <typename Func>
    optional_t<process> execute(Func func) noexcept
    {
        int status;
        // From `man 2 setns`:
        // "reassociating the calling thread with a PID namespace
        // changes only the PID namespace that child processes
        // of the caller will be created in."
        // Thus, we should call fork() once more.
        pid_t pid = ::fork();

        if (pid < 0) {
            return boost::none;
        }

        if (pid == 0) {
            // Join namespaces
            for (int fd: ns_fds_) {
                if (::setns(fd, 0) < 0) {
                    ::_exit(EXIT_FAILURE);
                }
            }

            // Up to this point, the current process
            // has joined all namespaces except PID namespace

            // The second fork produces new process which is placed into PID namespace
            pid = ::fork();
            if (pid < 0) {
                ::_exit(EXIT_FAILURE);
            }

            if (pid == 0) {
                if (!env_.enter()) {
                    ::_exit(EXIT_FAILURE);
                }

                try {
                    func();
                } catch(...) {
                    ::exit(EXIT_FAILURE);
                }
                ::exit(EXIT_SUCCESS);
            }

            ::waitpid(pid, &status, 0);
            ::exit(WEXITSTATUS(status));
        }

        return process(pid);
    }

    /**
     * Launches executable pointed to by @p path within existing container.
     *
     * @param   args    Arguments passed on the command line.
     *
     * @return Process or none.
     */
    optional_t<process> execute(const std::string &path,
            std::initializer_list<std::string> &&args) noexcept
    {
        return execute([&path, &args] () {
            // Casting out constness in this case is not as bad
            // as it might look at first because the entire process
            // will be replaced by execvp() anyway.
            std::vector<char *> exec_args(1, const_cast<char*>(path.c_str()));
            for (auto &a : args) {
                exec_args.emplace_back(const_cast<char*>(a.c_str()));
            }
            exec_args.emplace_back(nullptr);
            execvp(path.c_str(), exec_args.data());
            exit(EXIT_FAILURE);
        });
    }

    /**
     * Retrieves list of processes currently running in the container.
     */
    const std::vector<process> &ps();

private:
    std::vector<int> ns_fds_;

    stack_handle_t stack_;
    optional_t<process> init_process_;
    box_env env_;
};

} // namespace jewelbox

#endif // JEWELBOX_BOX_HPP
