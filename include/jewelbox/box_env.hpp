#ifndef JEWELBOX_ENV_HPP
#define JEWELBOX_ENV_HPP

#include <string>
#include <map>

#include <csignal>

namespace jewelbox
{

/**
 * Represents box environment.
 * The class is responsible for chroot()'ing, mounting procfs etc.
 */
class box_env
{
public:
    box_env(const std::string &box_name = "", const std::string &hostname = "",
            const std::map<std::string, std::string> &mount_points = {});
    ~box_env() = default;

    /**
     * Deploy configured environment into current box.
     *
     * @note Assumes to be called once per box (e.g in the init process).
     *
     * @return Status of operation.
     */
    bool deploy();

    /**
     * Clean up previously deployed environment.
     *
     * @note Assumes to be called once per box as it is going to be destroyed.
     */
    void cleanup();

    /**
     * Enters deployed environment.
     *
     * @note Assumes to be called once per process
     * which is about to execute within a box.
     *
     * @return Status of operation.
     */
    bool enter();

    /**
     * Retrieve absolute root path for the current environment.
     */
    const std::string &get_root_path() const noexcept { return root_path_; }

    /**
     * Set to @c SIGTERM when the @c SIGTERM signal is received for init process (thread).
     */
    static std::sig_atomic_t sigterm_flag;

private:

    bool mount_filesystems() noexcept;
    void unmount_filesystems() noexcept;

    std::string root_path_;
    std::string host_name_;
    std::map<std::string, std::string> mount_points_;
};

} // namespace jewelbox

#endif // JEWELBOX_ENV_HPP
