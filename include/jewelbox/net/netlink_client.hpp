#ifndef JEWELBOX_NETLINK_CLIENT_HPP
#define JEWELBOX_NETLINK_CLIENT_HPP

#include <sys/types.h>

#include <vector>
#include <list>
#include <cstdint>
#include <memory>

#include <boost/optional.hpp>

struct nlmsghdr;
struct iovec;

namespace jewelbox
{

namespace net
{

template <typename T>
using optional_t = boost::optional<T>;

class netlink_channel
{
public:
    netlink_channel(pid_t pid, int protocol_family):
        pid_(pid), protocol_family_(protocol_family)
    {}

    /**
     * Send data specified in the iovec structure
     *
     * @retval none Failure.
     */
    virtual optional_t<std::size_t> write(iovec *iov,
            std::size_t iov_length) noexcept = 0;

    /**
     * Receive data into the buffer.
     *
     * @param buffer Pointer to buffer of size at greater or equal to @p size
     * @param size Maximum size of read operation.
     *
     * @return Actual number of bytes read.
     * @retval none Failure.
     */
    virtual optional_t<std::size_t> read(std::uint8_t *buffer,
            std::size_t size) noexcept = 0;

    pid_t get_pid() const noexcept
    {
        return pid_;
    }

    int get_protocol_family() const noexcept
    {
        return protocol_family_;
    }

private:
    pid_t pid_;
    int protocol_family_;
};

using netlink_channel_ptr = std::shared_ptr<netlink_channel>;

class synchronous_netlink_channel: public netlink_channel
{
public:
    synchronous_netlink_channel(pid_t pid, int protocol_family);
    virtual ~synchronous_netlink_channel();

    optional_t<std::size_t> write(iovec *, std::size_t) noexcept override;

    optional_t<std::size_t> read(std::uint8_t *, std::size_t) noexcept override;
private:
    int sockfd_;
};

/**
 * A syncronous client for the Netlink protocol.
 *
* @todo Investigate integration with boost::asio.
 */
class netlink_client
{
public:
    netlink_client() = default;
    ~netlink_client() = default;

    /**
     * Sends synchronous request to the kernel Netlink service.
     * Filters synchronous response from the kernel by @resp_type
     * by saving the messages of given type into internal buffer.
     *
     * @tparam RequestData      Service-specific message type.
     *
     * @
     * @param nlm_flags     Additional flags to add to the Netlink message header.
     * @param req_type      Service-specific type of request message.
     * @param data          Pointer to the data of type @p RequestData.
     *
     * @note Size of the @data is deduced by @p sizeof(RequestData).
     *
     * @return List of pointers to filtered messages held in the internal client buffer.
     *
     * @note The internal buffer is invalidated on the next call to send_request.
     */
    template <typename RequestData>
    bool send_request(netlink_channel_ptr chan, int nlm_flags, int req_type,
            const RequestData *data) noexcept
    {
        return send_request(chan, nlm_flags, req_type,
                reinterpret_cast<const uint8_t *>(data), sizeof(RequestData));
    }

    /**
     * Non-template version of @p send_request.
     *
     * @param data_size Size of @p data in bytes.
     */
    bool send_request(netlink_channel_ptr chan, int nlm_flags, int req_type,
            const std::uint8_t *data, std::size_t data_size) noexcept;

    /**
     * Receives response through the provided Netlink channel.
     *
     * @param resp_type     Service-specific response type
     *                      used to filter response from the service.
     *
     * @return List of pointers to filtered messages held in the internal client buffer.
     */
    std::vector<const nlmsghdr *> receive_response(netlink_channel_ptr chan,
            int resp_type) noexcept;

private:
    std::list<std::vector<std::uint8_t>> response_cache_;
};

} // namepsace net

} // namespace jewelbox

#endif // JEWELBOX_NETLINK_CLIENT_HPP

