#ifndef JEWELBOX_NETDEVICE_HPP
#define JEWELBOX_NETDEVICE_HPP

#include <cstddef>
#include <string>
#include <array>
#include <vector>
#include <sstream>
#include <memory>

#include <jewelbox/net/rta.hpp>

#include <boost/optional.hpp>

struct ifinfomsg;

namespace jewelbox
{

namespace net
{

enum class netdevice_type
{
    netdevice_unknown,
    netdevice_lo,
    netdevice_eth,
    netdevice_veth,
    netdevice_bridge
};

using hw_addr_t = std::array<std::uint8_t, 6>;

std::ostream &operator<<(std::ostream &os, const hw_addr_t &hwaddr);

template <typename T>
using optional_t = boost::optional<T>;

class netlink_client;
class netlink_channel;
using netlink_channel_ptr = std::shared_ptr<netlink_channel>;

/**
 * iproute2-style interface into Linux networking device.
 *
 * See `man 7 netdevice`.
 */
class netdevice
{
public:
    /**
     * @param index     Network interface index.
     * @param nl_chan   Netlink channel used to interface with kernel networking stack.
     *
     */
    netdevice(int index, std::shared_ptr<netlink_channel> nl_chan);

    netdevice(const netdevice &other) = delete;
    netdevice &operator=(const netdevice &other) = delete;

    netdevice(netdevice &&other);
    netdevice &operator=(netdevice &&other);

    ~netdevice();

    /**
     * Retrieves latest device configuration.
     */
    bool fetch_data() noexcept;

    /**
     * Flushes device configuration to the kernel.
     */
    bool flush_data() noexcept;

    optional_t<std::string> get_name() const noexcept;

    bool set_name(const std::string &new_name) noexcept;

    bool is_up() const noexcept;

    bool set_up() noexcept;

    bool set_down() noexcept;

    optional_t<std::size_t> get_mtu() const noexcept;

    bool set_mtu(const std::size_t mtu) noexcept;

    optional_t<hw_addr_t> get_hw_addr() const noexcept;

    bool set_hw_addr(const hw_addr_t &new_addr) noexcept;

    netdevice_type get_type() const noexcept;

    int get_index() const noexcept
    {
        return index_;
    }

private:
    int index_;
    netlink_channel_ptr nl_channel_;
    netlink_client *nl_client_;
    ifinfomsg *if_config_;
    rta if_attributes_;
};

using netdevice_ptr = std::shared_ptr<netdevice>;

} // namespace net

} // namespace jewelbox

std::ostream &operator<<(std::ostream &os, const jewelbox::net::hw_addr_t &hwaddr);

#endif // JEWELBOX_NETDEVICE_HPP
