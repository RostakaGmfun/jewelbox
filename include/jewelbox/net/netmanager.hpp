#ifndef JEWELBOX_NETMANAGER_HPP
#define JEWELBOX_NETMANAGER_HPP

#include <jewelbox/net/netdevice.hpp>
#include <jewelbox/net/netlink_client.hpp>

#include <vector>
#include <string>

namespace jewelbox
{

namespace net
{

/**
 * An iproute2-style network interface manager.
 * Adds, deletes and configures Linux network interfaces.
 */
class netmanager
{
public:
    netmanager();
    ~netmanager() = default;

    /**
     * Retrieves a list of network devices.
     */
    std::vector<std::string> query_devices() noexcept;

    /**
     * Retrieves network device by name.
     */
    netdevice_ptr get_device(const std::string &name) noexcept;

    /**
     * Creates new network device with given @p name and @p type.
     */
    netdevice_ptr add_device(const std::string &name, netdevice_type type) noexcept;

    /**
     * Delete network interface with given @p name.
     *
     * @return Status of operation.
     */
    bool delete_device(const std::string &name) noexcept;

    /**
     * Retrieve the NET namespace file descriptor.
     */
    int get_netns_fd() const noexcept
    {
        return netns_fd_;
    }

private:
    netlink_channel_ptr nl_channel_;
    netlink_client rtnet_client_;
    std::map<std::string, int> device_cache_;
    int netns_fd_;
};

} // namespace net

} // namespace jewelbox

#endif // JEWELBOX_NETMANAGER_HPP
