#ifndef JEWELBOX_RTA_HPP
#define JEWELBOX_RTA_HPP

#include <linux/rtnetlink.h>

#include <cstddef>
#include <tuple>
#include <vector>
#include <map>

namespace jewelbox
{

namespace net
{

/**
 * Interface to @p NETLINK_ROUTE attributes.
 */
class rta
{
public:
    using rta_type_t = unsigned short;
    /**
     * @param start Pointer to first attribute.
     * @param length Total length of attribute array.
     *
     * In case @p nullptr is passed, the attribute array will be empty.
     */
    rta(const rtattr *start = nullptr, std::size_t length = 0);

    void reset(const rtattr *start, std::size_t length) noexcept;

    /**
     * Retrieves attribute of given type.
     *
     * @param type RTA type. @see `man 7 rtnetlink`
     *
     * @return Size of attribute and pointer to it.
     * @retval (0, nullptr) if attribute does not exist.
     */
    std::tuple<std::size_t, const void*> get(rta_type_t type) const noexcept;

    /**
     * Adds attribute to the attribute array.
     *
     * @tparam AttributeType Type of attribute data.
     *
     * @param type RTA type.
     * @param attr Pointer to the attribute data.
     *
     * @note Size of attribute data is deduced as @p sizeof(AttributeType).
     * @note In case attribute exists, the data will be overwritten.
     */
    template <typename AttributeType>
    void add(int type, const AttributeType *attr) noexcept
    {
        add(type, attr, sizeof(AttributeType));
    }

    /**
     * Non-template version of @p add().
     *
     * @param type RTA type.
     * @param attr Pointer to attribute data.
     * @param size Size of attribute data.
     */
    void add(rta_type_t type, const void *attr, std::size_t size) noexcept;

    /**
     * Returns pointer to the first attrbiute in the array.
     *
     * @note The pointer might be iinvalidated after next call to @p data().
     */
    const rtattr *data() const noexcept;

    std::size_t size() const noexcept;

private:
    void update_attribute_array_cache() const noexcept;

    void parse_rtattr(const rtattr *start, std::size_t length) noexcept;

    mutable bool dirty_flag_;
    mutable std::vector<std::uint8_t> attribute_array_cache_;

    std::map<rta_type_t , std::vector<std::uint8_t>> attributes_;
};

} // namespace net

} // namespace jewelbox

#endif // JEWELBOX_RTA_HPP
